# -*- coding: utf-8 -*-
from flask import Flask, render_template, jsonify, send_file, flash,session
import re
import os
from flask import request, flash
import requests
import json
import time
from werkzeug.utils import secure_filename
import concurrent.futures
import time
import nltk
os.environ['OMP_THREAD_LIMIT'] = '1'
app = Flask(__name__)
@app.route('/', methods=['GET', 'POST'])
def index():
	result=''
	return render_template('index.html')

@app.route('/uploader', methods = ['GET', 'POST'])
def upload_file():
	with concurrent.futures.ProcessPoolExecutor(max_workers=16) as executor:
		
		if request.method == 'POST':
			file= request.files['file']		
			file.save(secure_filename(file.filename))
			filename1=file.filename
			cmd= "convert -density 300 "+filename1+ " -depth 8 -strip -background white -alpha off "+filename1+".tiff"
			os.system(cmd)
			file_name=filename1+".tiff"
			cmd2= "tesseract "+file_name+" -l mal+eng "+file_name
			os.system(cmd2)
			filet=file_name+".txt"
			fil=open(filet)
			lines=fil.readlines()
			r1=''
			for line in lines:

				line=line.strip("\n")
				r1=r1+line+'\n'
			result=r1
			result1=[result]
			print(result)
			cmd3= "mv "+" "+filename1+" "+file_name+" "+filet+" "+"filess"
			os.system(cmd3)
			return jsonify(result1)
if __name__ == '__main__':
	app.debug = True
	app.run(host = '0.0.0.0',port=8000,threaded=True)
