# Malayalam OCR using Tesseract
# DHRITI - Document Handling & Retrieval of Image to Text Information
Converts Malayalam typed documents into editable text.
* Based on Tesseract OCR engine
* Multi-Lingual recognition : English + Malayalam
* Input file formats : JPEG, TIFF, PDF, PNG
* Output file format : Plain text (TXT)

### Prerequisites
* [ImageMagick](https://linuxconfig.org/how-to-install-imagemagick-7-on-ubuntu-18-04-linux)
* [Tesseract](https://www.howtoforge.com/tutorial/tesseract-ocr-installation-and-usage-on-ubuntu-16-04/)

### Dependencies:   
1. Python 3.6 
2. ImageMagick
3. PyPI dependencies listed in `requirements.txt`

### Installation of PyPI dependencies:
``` 
pip install -r requirements.txt  
```

## Built With
* [Tesseract](https://github.com/tesseract-ocr/tesseract) The Tesseract framework used
* Tesseract License : Apache 2.0 License

* Reference : [Tesseract](https://diging.atlassian.net/wiki/spaces/DCH/pages/5275668/Tutorial+Text+Extraction+and+OCR+with+Tesseract+and+ImageMagick)
* Flask : Web Development

## Creators
* **Sandhini S, Research Assistant, Language Technology, ICFOSS** -  [Sandhini S](https://gitlab.com/Sandhinisukumar)

## Project Guidance and Support
Dr. Rajeev R R , Programme Head, Language Technology, ICFOSS

## Developers
This project is Developed by ICFOSS

## License
This project is licensed under the GNU - GPL v3 - see the (https://www.gnu.org/licenses/gpl-3.0.en.html) file for details